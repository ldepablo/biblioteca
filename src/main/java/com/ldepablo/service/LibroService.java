package com.ldepablo.service;

import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ldepablo.model.Libro;
import com.ldepablo.repository.LibroRepository;

@Service
public class LibroService {

	private final LibroRepository repo;
	
	@Autowired
	public LibroService(LibroRepository libroRepository){
		this.repo = libroRepository;
	}
	
	@Transactional
	public Collection<Libro> getLibrosCaducados() {
		long millis = System.currentTimeMillis();
		
		// Aqu'i implementar'iamos cualquier l'ogica de negocio
		// En este caso, es muy directo. Aplico un poc de clean coding.
		return repo.findByfechaDevolucionLessThan(millis);
	}
}
