package com.ldepablo.controller;


import java.util.Collection;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ldepablo.model.Libro;
import com.ldepablo.service.LibroService;

@Controller
public class LibroController {
	
	private LibroService service;

	@Autowired
	public LibroController(LibroService libroService) {
		this.service = libroService;
	}
	
	@RequestMapping(value={"/", "/libro/caducados"})
	public String list(
			Model model,
			HttpSession session) {
		
		Collection<Libro> librosCaducados = service.getLibrosCaducados();
		
		model.addAttribute("libros", librosCaducados);
		

		return "/libro/caducados";
	}
	
}
