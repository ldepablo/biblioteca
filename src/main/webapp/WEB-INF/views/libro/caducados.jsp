<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Libros</title>

<link href="${pageContext.request.contextPath}/static/css/main.css" rel="stylesheet" type="text/css" />

</head>

<body>
		<h1>
			Caducados
		</h1>


	<table border="1">
		<tr>
			<th>libro</th>
			<th>fechaCaducidad</th>
		</tr>
		<c:forEach items="${libros.values()}" var="libros">
			<tr>
				<td>${libro.getName()}</td>
				<td>${libro.getFechaCaducidad}</td>
			</tr>
		</c:forEach>
	</table>

</body>
</html>