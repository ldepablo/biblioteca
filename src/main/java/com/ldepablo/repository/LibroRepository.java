package com.ldepablo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import com.ldepablo.model.Libro;

@NoRepositoryBean
public interface LibroRepository extends JpaRepository<Libro, Long> {

	 public List<Libro> findByfechaDevolucionLessThan(Long millis);
	
}
