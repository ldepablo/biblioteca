package com.ldepablo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class Libro {

	private Long id;
	
	private String name;
	
	// Lo correcto ser'ia crear otra tabla con los pr'estamos, pero para ir m'as r'apido, lo implemento as'i.
	private Long fechaDevolucion;
	

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Column
	public Long getFechaDevolucion() {
		return fechaDevolucion;
	}
	public void setFechaDevolucion(Long fechaDevolucion) {
		this.fechaDevolucion = fechaDevolucion;
	}
	
	
}
